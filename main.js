const getElement = id => document.getElementById(id);

const innerContent = (id, content) => getElement(id).innerText = content

const kiemTraSoNT = nt =>{
    if(nt < 2) return 0;
    for(let i = 2; i <= Math.sqrt(nt);i++) if(nt%i==0) return 0
    return 1
}

getElement('btn-count').addEventListener('click', () => {
    let chan = "", le = ""
    for (let count = 0; count < 100; count++)
        count % 2 == 0 ? chan += count + " " : le += count + " "
    innerContent('result-count', `Số chẵn: ${chan}
                                 Số lẻ: ${le}`)
})

getElement('btn-chia').addEventListener('click', () => {
    let count = 0;
    for (let n = 0; n < 1000; n = n + 3)
        count++;
    innerContent('result-chia', `Có tất cả ${count} số chia hết cho 3 nhỏ hơn 1000`)
})

getElement('btn-min').addEventListener('click', () => {
    let tong = 0, min = 0
    for (let tong = 0, n = 1; n < 10000; n++) {
        tong += n;
        if (tong > 10000) {
            min = n;
            break;
        }
    }
    innerContent('result-min', `Số nguyên dương nhỏ nhất: ${min}`)
})

getElement('btn-sum').addEventListener('click', () => {
    let x = getElement('x').value,
        n = getElement('n').value,
        sum = 0
    for (let i = 1; i <= n; i++) sum += Math.pow(x, i);
    innerContent('result-sum', `Tổng: ${sum}`)
})

getElement('btn-giaiThua').addEventListener('click', () => {
    let gt = getElement('giaithua').value, s = 1
    if (gt === 0) {
        s = 1;
    }
    for (let n = 1; n <= gt; n++) {
        s *= n;
    }
    innerContent('result-giaiThua', `Giai thừa: ${s}`)
})

getElement('btn-div').addEventListener('click', () => {
    let strContent = ""
    for (let i = 0; i < 10; i++)
        strContent += (i % 2 == 0) ?
            `<div class="bg-danger p-2">Div thứ ${i}</div>`
            : `<div class="bg-info p-2">Div thứ ${i}</div>`;
    getElement('result-div').innerHTML = strContent
})

getElement('btn-nt').addEventListener('click', () => {
    let nt = getElement('nt').value,
    strNT = ''
    for(let n = 1; n <= nt; n++){
        if(kiemTraSoNT(n)) strNT += ' ' + n
    }
    innerContent('result-nt',strNT)
})